﻿using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;

namespace NewsFeed.models
{
    public class Rootobject
    {
        public List<Article> articles { get; set; }
    }

    public class Article
    {
        public string title { get; set; }
        public string website { get; set; }
        public string authors { get; set; }
        public string date { get; set; }
        public string content { get; set; }
        public Tag[] tags { get; set; }
        public string image_url { get; set; }
        public object BitmapCacheOption { get; private set; }

        public async Task<BitmapImage> GetImageAsync() {

            BitmapImage BitmapImage = null;

            HttpClient httpClient = new HttpClient();

            using (HttpResponseMessage response = await httpClient.GetAsync(image_url))
            {
                if (response.IsSuccessStatusCode)
                {

                    using (var stream = await response.Content.ReadAsStreamAsync())
                    {
                        using (var memStream = new MemoryStream())
                        {
                            await stream.CopyToAsync(memStream);
                            memStream.Position = 0;
                            BitmapImage = new BitmapImage();
                            BitmapImage.SetSource(memStream.AsRandomAccessStream());
                        }

                    }
                }
            }
            return BitmapImage;
        }
    }

    public class Tag
    {
        public int id { get; set; }
        public string label { get; set; }
    }


}