﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using NewsFeed.models;
using System.Collections.ObjectModel;
using Windows.UI.Core;
using Windows.UI.Xaml.Media.Imaging;


namespace NewsFeed
{

    public sealed partial class MainPage : Page
    {
        public ObservableCollection<Article> articles { get; } = new ObservableCollection<Article>();
        private Article persistedItem;

        public MainPage()
        {
            this.InitializeComponent();
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            
            SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;

            if (articles.Count == 0)
            {
                await GetArticles();
            }

            base.OnNavigatedTo(e);
        }

        // Handle Item click event 
        private void ArticalGridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            // get the selected item
            persistedItem = e.ClickedItem as Article;
            this.Frame.Navigate(typeof(DetailPage), e.ClickedItem);
        }

        private async Task GetArticles()
        {

            string url = "https://api.myjson.com/bins/df5bi";

            HttpClient client = new HttpClient();

            string response = await client.GetStringAsync(url);

            var data = JsonConvert.DeserializeObject<Rootobject>(response);

            foreach (Article artical in data.articles)
            {
                articles.Add(artical);
            }

        }

        private async void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            await GetArticles();
        }

        private void ArticalGridView_ContainerContentChanging(ListViewBase sender, ContainerContentChangingEventArgs args) {
            if (args.InRecycleQueue)
            {
                var templateRoot = args.ItemContainer.ContentTemplateRoot as Grid;
                var image = (Image)templateRoot.FindName("ArticleImage");

                image.Source = null;
            }

            if (args.Phase == 0)
            {
                args.RegisterUpdateCallback(ShowImage);
                args.Handled = true;
            }
        }

        private async void ShowImage(ListViewBase sender, ContainerContentChangingEventArgs args)
        {
            if (args.Phase == 1)
            {
                // It's phase 1, so show this item's image.
                var templateRoot = args.ItemContainer.ContentTemplateRoot as Grid;
                var image = (Image)templateRoot.FindName("ArticleImage");
                image.Opacity = 100;

                var item = args.Item as Article;

                try
                {
                    image.Source = await item.GetImageAsync();
                }
                catch (Exception)
                {
                    // File could be corrupt, or it might have an image file
                    // extension, but not really be an image file.
                    BitmapImage bitmapImage = new BitmapImage();
                    bitmapImage.UriSource = new Uri(image.BaseUri, "Assets/StoreLogo.png");
                    image.Source = bitmapImage;
                }
            }
        }

    }
}
